// Takes the data from the movie API and converts it to the format required by Pex
import movieData from '../data/movieDetails.json';
import castData from '../data/castDetails.json';
import genreData from '../data/genres.json';
import { writeFileSync } from 'fs';

/*
expected:
{
  "id": 163982121944357888,
  "title": "The Notebook",
  "genre": [
    {
      "id": 2,
      "title": "Drama"
    },
    {
      "id": 6,
      "title": "Romance"
    }
  ],
  "actors": [
    {
      "id": 163982182073900544,
      "name": "Ryan Gosling"
    },
    {
      "id": 163982199253770240,
      "name": "Rachel McAdams"
    }
  ],
  "is_series": false,
  "release_date": "2004-06-25T00:00:00.000000Z"
}
*/

const getGenreData = (genreIds: number[]) => {
  return genreIds.map((id) => genreData.find((g) => g.id === id));
};

const getActorData = (movieId: number) => {
  // Only return top four actors per movie
  return (castData as any[])
    .find((cd) => cd.id === movieId)
    .cast.slice(0, 4)
    .map((actor: any) => ({
      id: actor.id,
      name: actor.name,
    }));
};

const convertReleaseDate = (date: string | undefined) =>
  `${date || '1900-01-01'}T00:00:00.000000Z`;

const foundMovies: number[] = [];
// This is O(yikes) but only needs to be run once ever
const pexData = movieData
  // filter out dupes
  .filter((movie) => {
    if (foundMovies.includes(movie.id)) {
      return false;
    }
    foundMovies.push(movie.id);
    return true;
  })
  .map((movie) => {
    return {
      id: movie.id,
      title: movie.title,
      genre: getGenreData(movie.genre_ids),
      actors: getActorData(movie.id),
      // as far as I can tell, the API doesn't provide this
      // and I'm not manually going through 1000 movies to add it
      is_series: false,
      release_date: convertReleaseDate(movie.release_date),
      poster: movie.poster_path,
      description: movie.overview,
    };
  });

// Place it in /public so that the frontend can request it
writeFileSync('./public/movieData.json', JSON.stringify(pexData));
