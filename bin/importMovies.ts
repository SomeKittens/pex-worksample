import fetch from 'node-fetch';
import { writeFileSync } from 'fs';
import topThousand from '../data/imdbTopThousand.json';

require('dotenv').config();

const apiURL = 'https://api.themoviedb.org/3';

const movieRequest = async (
  url: string,
  query: { [key: string]: string } = {}
) => {
  query.api_key = process.env.API_KEY as string;
  const queryString = Object.entries(query)
    .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
    .join('&');

  return await (
    await fetch(`${apiURL}${url}?${queryString}`, {
      method: 'GET',
    })
  ).json();
};

const fetchMovie = async (movieName: string) => {
  const response = await movieRequest('/search/movie', { query: movieName });
  if (!response.results.length) {
    throw new Error(`No results for: ${movieName}`);
  }
  return response.results[0];
};

const fetchCast = async (movieId: number) => {
  return await movieRequest(`/movie/${movieId}/credits`);
};

const fetchImage = async (movieImage: string) => {
  const image = await fetch(
    `https://www.themoviedb.org/t/p/w220_and_h330_face/${movieImage}`
  );
  const buffer = await image.buffer();
  writeFileSync(`./public/posters/${movieImage}`, buffer);
};

// They disabled rate limiting, the fools!
console.log('Fetching movies...');
Promise.all(topThousand.map(fetchMovie))
  .then((movieDetails) => {
    console.log('Movies fetched');
    // Save intermediate data
    writeFileSync('./data/movieDetails.json', JSON.stringify(movieDetails));

    console.log('Fetching cast & posters...');
    return Promise.all(
      movieDetails.map(async (movie) => {
        await fetchImage(movie.poster_path);
        return await fetchCast(movie.id);
      })
    );
  })
  .then((castDetails) => {
    console.log('Cast and posters fetched');
    writeFileSync('./data/castDetails.json', JSON.stringify(castDetails));

    console.log('Next, run npm run convert-movies');
  });
