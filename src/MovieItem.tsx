import { VideoCameraOutlined } from '@ant-design/icons';
import { Card, Divider } from 'antd';
import React from 'react';
import { Movie } from './types';

import './MovieItem.less';

const { Meta } = Card;

interface Props {
  movie: Movie;
}

// Renders information about a single movie
const MovieItemFn: React.FC<Props> = ({ movie }) => {
  return (
    <Card
      className="movie-item"
      cover={
        <img
          className="movie-poster"
          alt={`Poster for ${movie.title}`}
          src={`/posters${movie.poster}`}
        />
      }
      actions={[
        <a
          // No particular affiliation, found through Google
          href={`https://www.justwatch.com/us/search?q=${encodeURIComponent(
            movie.title
          )}`}
          target="_blank"
        >
          <VideoCameraOutlined key="find-streams" />
        </a>,
      ]}
    >
      <Meta
        title={movie.title}
        description={
          <>
            <Divider plain>Cast</Divider>
            <div className="actor-list">
              {movie.actors.map((actor) => {
                return <div key={actor.name}>{actor.name}</div>;
              })}
            </div>
          </>
        }
      />
    </Card>
  );
};

export const MovieItem = React.memo(
  MovieItemFn,
  (prevProps, nextProps) => prevProps.movie.id === nextProps.movie.id
);
