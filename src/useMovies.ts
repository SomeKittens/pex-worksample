import { useEffect, useState } from 'react';
import { Movie } from './types';

// Returns an array of notable strings
const pluckMovieDetails = (movie: Movie) => {
  return [
    movie.title,
    ...movie.genre.map((g) => g.title),
    ...movie.actors.map((a) => a.name),
  ].map((str) => str.toLowerCase());
};

// Custom hook to access movies
// Handles loading/error states
// WARNING: This is not a singleton, every time a component calls useMovies, it'll create a new instance
export const useMovies = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<Error>();
  // All movies
  const [movieList, setMovieList] = useState<Movie[]>([]);
  const [query, setQuery] = useState<string>();
  // Only movies that include the current query
  const [filteredMovies, setFilteredMovies] = useState<Movie[]>([]);

  useEffect(() => {
    // load movies
    fetch('/movieData.json')
      .then((res) => res.json())
      .then((movies: Movie[]) => {
        // One of the worksample requirements was to log the data structure
        console.log(movies);
        setMovieList(
          movies
            .map((movie) => ({
              ...movie,
              details: pluckMovieDetails(movie),
            }))
            // Only take 100 since this is just a worksample
            // In a production app, add pagination or the like
            .slice(0, 100)
        );
      })
      .catch(setError)
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    if (!movieList) {
      return;
    }
    if (!query) {
      setFilteredMovies(movieList);
      return;
    }

    const lcQuery = query.toLowerCase();

    // Filter the list
    setFilteredMovies(
      movieList.filter((movie) =>
        movie.details.some((detail) => detail.includes(lcQuery))
      )
    );
  }, [query, movieList]);

  return {
    loading,
    error,
    movies: filteredMovies,
    query,
    setQuery: setQuery,
  };
};
