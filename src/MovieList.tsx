import React from 'react';
import { Empty, Row } from 'antd';
import { MovieItem } from './MovieItem';
import { Movie } from './types';

import './MovieList.less';

// Given a list of movies, render them
export const MovieList: React.FC<{ movies: Movie[] }> = ({ movies }) => {
  if (!movies.length) {
    return <Empty description="No movies found!" />;
  }

  return (
    <Row justify="space-around" align="middle" className="movie-list">
      {movies?.map((movie) => {
        return <MovieItem movie={movie} key={movie.id} />;
      })}
    </Row>
  );
};
