export interface Movie {
  id: number;
  title: string;
  genre: {
    id: number;
    title: string;
  }[];
  actors: {
    id: number;
    name: string;
  }[];
  is_series: boolean;
  release_date: string;
  poster: string;
  description: string;
  details: string[];
}
