import React from 'react';
import { Alert, Col, Input, Layout, Row, Spin } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { useMovies } from './useMovies';
import { MovieList } from './MovieList';

import './App.less';

const { Header, Content } = Layout;

// Root app component, central clearinghouse for data and app state
// If this was a non-trivial app, we'd want to break this up
const App: React.FC = () => {
  const { loading, error, movies, query, setQuery } = useMovies();

  let content: React.ReactNode;

  if (loading) {
    content = (
      <Row justify="space-around" align="middle" className="loading-spinner">
        <Col>
          <Spin />
        </Col>
      </Row>
    );
  } else if (error) {
    content = (
      <Row justify="space-around" align="middle" className="error-message">
        <Col>
          <Alert
            message="Something went wrong!"
            description={error?.message}
            type="error"
            showIcon
          />
        </Col>
      </Row>
    );
  } else {
    content = <MovieList movies={movies} />;
  }

  return (
    <Layout className="app">
      <Header className="app-header">
        {/* todo: search bar */}
        <Input
          size="large"
          prefix={<SearchOutlined />}
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        />
      </Header>
      <Content className="app-content">{content}</Content>
    </Layout>
  );
};

export default App;
