# PEX Worksample

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

Run `npm install` and `npm start` to get the application up and running.  Type something in the search bar to see results!

Or, just go to https://pex-worksample.netlify.app/ and play around there.

## Importing data

Data's already imported for you, but just in case you wanted to try it for yourself (not recommended):

### Get list of top 1000 movies from IMDB

Load up the list [here](https://www.imdb.com/search/title/?groups=top_1000&sort=user_rating,desc&count=100&view=simple).  Open up the dev console in Chrome and paste in the following command:

```
copy(Array.from($('.col-title a')).map(node => node.innerText))
```

That'll copy the list of titles to your clipboard as a JSON array.  Paste them somewhere.  Attempt to show all results on a single page by editing the URL, discover IMDB doesn't let you do that.  Sigh, and go through the remaining nine pages, running the above command and pasting the results somewhere.  Edit the file so it's a flat JSON array of strings, then save it as `imdbTopThousand.json` in the project root.  Also, "Elite Squad 2: The Enemy Within" is wrong?  It's actually called "Elite Squad: The Enemy Within".  Manually edit that change in.

### Import movie data from TheMovieDB

Next, go to themoviedb.org and sign up for an account and then an API key (IMDB has an API but it's $$$).  Create a `.env` file in the project root and add the line `API_KEY=1234abcd`, substituting your API key for the obviously fake one.

Once that's done, manually edit the `module` property in `tsconfig.json` to `"CommonJS"`.  The React server overwrites this property whenever it starts and we need this setting to run `ts-node`.  Run `npm run import-movies`.  Then run `npm run convert-movies`.

That'll probably work (at least on my machine).  You may need to muck around a bit with the data manually.

I told you it wasn't recommended to do this yourself.
