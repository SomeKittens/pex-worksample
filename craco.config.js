const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              // overrides go here
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
